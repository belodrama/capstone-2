const User = require('../models/user');
const Product = require('../models/product');
const auth = require('../auth');
const bcrypt = require('bcrypt'); 


module.exports.registerUser = (body) => {

	let newUser = new User({
		userName: body.userName,
		password: bcrypt.hashSync(body.password, 10),
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		gender: body.gender
	})

	return User.find({userName: body.userName}).then(result => {
		if(result.length == 0){
			return newUser.save().then() 
		}else{
			return "Duplicate username"
		}
	})
}


module.exports.loginUser = (body) => {
	return User.findOne({userName: body.userName}).then(result => {
		if(result === null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())
				}
			}else{
				return false;
			}
		}
	})
}


module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined; 
		return result;
	})
}


module.exports.admin = (body) => {
	let admin = {isAdmin: true}

	return User.findByIdAndUpdate(body.userId, admin).then((user, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}


module.exports.makeAdmin = (body) => {

	let makeAdmin = {isAdmin: true}
	
	return User.findByIdAndUpdate(body.userId, makeAdmin).then((user, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})

}
