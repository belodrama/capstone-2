const User = require('../models/user');
const Product = require('../models/product');
const auth = require('../auth');
const bcrypt = require('bcrypt'); 


module.exports.addProduct = (body) => { 

	let newProduct = new Product({
		name: body.name,
		description: body.description,
		category: body.category,
		price: body.price
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})

}


module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}


module.exports.getInStock = () => {
	return Product.find({isAvailable: true}).then(result => {
		return result;
	})
}


module.exports.getProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result;
	})
}



module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		category: body.category,
		price: body.price
	}
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((course, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}


module.exports.archiveProduct = (params) => {
	let obsoleteProduct = {
		isActive: false,
		isAvailable: false,
	}
	return Product.findByIdAndUpdate(params.productId, obsoleteProduct).then((course, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}











