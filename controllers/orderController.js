const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');
const auth = require('../auth');
const bcrypt = require('bcrypt'); 



module.exports.checkout = (body, userId) => { 

	let newOrder = new Order({
		userId: userId,
		purchasedItem: body.purchasedItem,
		noOfItems: body.noOfItems,
		totalAmount: body.totalAmount,
		deliveryOption: body.deliveryOption,
		payment: body.payment,
		deliveryAddress: body.deliveryAddress,
		status: body.status	
	})

	return newOrder.save().then((order, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


module.exports.getOwnOrder = (buyer) =>{
	return Order.find({userId: buyer}).then(result => {
		return result
	})
}


module.exports.getAll = () => {
	return Order.find().then(result => {
		return result
	})
}



