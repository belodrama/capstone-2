const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/orderController');


router.post('/checkout', auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === false){
		orderController.checkout(req.body, token.id).then(resultFromCheckout => res.send(resultFromCheckout))
	}else{
		res.send({auth: "Should not be an admin"})
	}
})


router.get('/myOrder', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.getOwnOrder(userData.id).then(resultFromGetOrder => res.send(resultFromGetOrder))
})


router.get('/all', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		orderController.getAll().then(resultFromGetAll => res.send(resultFromGetAll))
	}else{
		res.send({auth: "Not an admin"})
	}
})



module.exports = router;