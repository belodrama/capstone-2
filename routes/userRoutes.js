const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/userController');


router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})


router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})


router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})


router.put('/admin', (req, res) => {
	userController.admin(req.body).then(resultFromAdmin => res.send(resultFromAdmin))
})


router.put('/makeAdmin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
		userController.makeAdmin(req.body).then(resultFromSetAdmin =>res.send(resultFromSetAdmin))
	}else{
		res.send({auth: "Unable to set as admin."})
	}
})


module.exports = router;