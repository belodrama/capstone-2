const express = require('express');
const router = express.Router();
const auth = require('../auth');
const productController = require('../controllers/productController');


router.post('/', auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})


router.get('/all', (req, res) => {
	productController.getAllActive().then(resultFromGetAllActive => res.send(resultFromGetAllActive))
})


router.get('/instock', (req, res) => {
	productController.getInStock().then(resultFromInStock => res.send(resultFromInStock))
})


router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromGetProduct => res.send(resultFromGetProduct))
})


router.put('/:productId', auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
		productController.updateProduct(req.params, req.body).then(resultFromUpdateProduct => res.send(resultFromUpdateProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})


router.delete('/:productId', auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
	productController.archiveProduct(req.params).then(resultFromArchiveProduct => res.send(resultFromArchiveProduct))
	}else{
		res.send({auth: "Not an admin"})
	}
})


module.exports = router;