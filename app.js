const express = require("express");
const mongoose = require("mongoose");

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');


const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

mongoose.connect("mongodb+srv://admin:admin123@cluster0.vpntk.mongodb.net/carlos_ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Altas.")
})

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
})