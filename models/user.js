const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	userName: {
		type: String,
		required: [true, "Username is required."]
	},	
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	firstName: {
		type: String,
		required: [ true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [ true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	gender: {
		type: String,
		required: [true, "Gender is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
})


module.exports = mongoose.model("User", userSchema);