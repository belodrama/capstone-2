const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: true
	},

	purchasedItem: [
		{
			productId: {
				type: String,
				required: true
			},	
			quantity: {
				type: Number,
				required: true
			},
			specialInstructions: {
				type: String,
				required: false //Optional
			}
		}
	],

	noOfItems: {
		type: Number,
		required: [true, "Number of items is required."]
	},

	totalAmount: {
		type: Number,
		required: true
	},

	deliveryOption: { //pickup or delivery
		type: String,
		required: [true, "Delivery option is required."]
	},

	payment: [
		{
			mode: {
				type: String,
				required: [true, "Mode of payment is required."] //Payment upon pickup or cash on delivery
			},

			deliveryCharge: {
				type: Number,
				required: [true, "Delivery Charge is required."]
			},
			
			total: {
				type: Number,
				required: true
			},

		}
	],


	
	deliveryAddress: [
		{
			fullName: {
				type: String,
				required: [true, "Recipient's Name is required."]
			},

			mobileNo: {
				type: String,
				required: [true, "Mobile Number is required."]
			},

			houseNo: {
				type: String,
				required: [true, "House or Unit Number is required."]
			},

			streetName: {
				type: String,
				required: [true, "Street or Building Name is required."]
			},

			town: {
				type: String,
				required: [true, "City is required."]
			},

			direction: { //Landmarks or notes
				type: String,
				required: false //Optional
			}

		}
	],
	
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	
	status: {
		type: String,
		default: "active" // active, closed
	}	
})


module.exports = mongoose.model("Order", orderSchema);